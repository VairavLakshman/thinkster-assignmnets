var myApp = angular.module('myApp', []);
myApp.controller('MainController', ['$scope', function($scope) {
    $scope.timeOfDay = 'morning';
    $scope.name = 'Vairav';
}]);
myApp.controller('ChildController', ['$scope', function($scope) {
    $scope.name = 'Mani';
}]);
myApp.controller('GrandChildController', ['$scope', function($scope) {
    $scope.timeOfDay = 'evening';
    $scope.name = 'Lakshmanan';
}]);