angular.module('filterInControllerModule', [])
    .controller('FilterController', ['filterFilter', function FilterController(filterFilter) {
        this.array = [
            {name: 'Tobias'},
            {name: 'Jeff'},
            {name: 'Brian'},
            {name: 'Igor'},
            {name: 'James'},
            {name: 'Brad'},
            {name: 'Vairav'},
            {name: 'Arun'}
        ];
        this.filteredArray = filterFilter(this.array, 'a');
    }]);