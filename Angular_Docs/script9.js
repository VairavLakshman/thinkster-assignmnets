angular.module("scopeExample", [])
    .controller("GreetController", ["$scope", '$rootScope', function($scope, $rootScope) {
        $scope.name = "world";
        $rootScope.department = 'angular';
    }])
    .controller("ListController", ["$scope", function($scope) {
        $scope.names = ['Ramanath', "Lakshman", "Vairav"];
//        $scope.department = "node";
    }]);