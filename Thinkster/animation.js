function TestController() {
    var self = this;
    self.showBoxOne = false;
    self.people = [
        {name: "Eric simons"},
        {name: "Albert Pai"},
        {name: "Mathhew Greenster"}
    ];
    self.bigger = false;
    self.lightTheme = false;
}
var app = angular.module('app', ['ngAnimate']);
app.controller("TestController", TestController);