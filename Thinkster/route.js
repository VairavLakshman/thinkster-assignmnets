var app = angular.module("myApp", ['ngRoute']);
app.config(function($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: 'routeApp.html',
        controller: 'AppController',
        resolve: {
            loadData: appController.loadData,
            prepData: appController.prepData
        }
    });
});
var appController = app.controller("AppController", function($scope) {
    $scope.model = {
        message: "I'm a great app"
    };
});
appController.loadData = function ($q, $timeout) {
    var defer = $q.defer;
    $timeout(function () {
        defer.resolve();
        console.log("loadData");
    }, 2000);
    return defer.promise;
};
appController.prepData = function ($q, $timeout) {
    var defer = $q.defer;
    $timeout(function () {
        defer.resolve();
        console.log("prepData")
    }, 2000);
    return defer.promise;
};