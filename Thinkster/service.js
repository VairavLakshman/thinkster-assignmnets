var app = angular.module('app', []);
app.factory('messages', function() {
    var messages = {};
    messages.list = [];
    messages.add = function(message) {
        messages.list.push({id: messages.list.length, text: message});
    };
    return messages;
});
app.controller("ListController", function(messages) {
    var self = this;
    self.messages = messages.list;
});
app.controller("PostController", function(messages){
    var self = this;
    self.newMessage = "Hello World!";
    self.addMessage = function(message) {
        messages.add(message);
        self.newMessage = '';
    };
});