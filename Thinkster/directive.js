function FunController() {
    var self = this;
    self.start = function() {
        console.log("Fun times have been started!");
    }
    self.end = function() {
        console.log("Fun time is over.");
    }
}

angular.module("greetings", [])
    .controller("FunController", FunController)
    .directive("entered", function() {
        return function(scope, element, attrs) {
            element.bind("mouseenter", function(){
                scope.$apply(attrs.entered);
            })
        }
    })
    .directive("first", function() {
        return {
            restrict: "E",
            scope: {},
            transclude: true,
            template: "<div>This is the First Component</div><ng-transclude></ng-transclude>"
        };
    })
    .directive('welcome', function () {
        return {
            restrict: "E",
            scope: {},
            controller: function($scope) {
                $scope.words = [];
                this.sayHello = function() {
                    $scope.words.push("hello");
                };
                this.sayHowdy = function() {
                    $scope.words.push("howdy");
                };
                this.sayHi = function() {
                    $scope.words.push("hi");
                };
            },
            link: function(scope, element) {
                    element.bind("mouseenter", function() {
                    console.log(scope.words);
                });
            }
//            template: "<div>Howdy there! You look splendid.</div>"
        };
    })
    .directive("hola", function() {
        return {
            require: "welcome",
            link: function (scope, element, attrs, welcomeController) {
                welcomeController.sayHello();
            }
        };
    })
    .directive("howdy", function() {
        return {
            require: "welcome",
            link: function (scope, element, attrs, welcomeController) {
                welcomeController.sayHowdy();
            }
        };
    })
    .directive("hoi", function() {
        return {
            require: "welcome",
            link: function (scope, element, attrs, welcomeController) {
                welcomeController.sayHi();
            }
        };
    })
    .directive('hi', function() {
        return {
            restrict: "A",
            link: function() {
                alert("Howdy!");
            }
        };
    })
    .directive('hey', function() {
        return {
            restrict: "A",
            link: function() {
                alert("there!")
            }
        }
    })
    .directive('hello', function() {
        return {
            restrict: "C",
            link: function() {
                alert("You look splendid!");
            }
        };
    })
    .directive('goodbye', function() {
        return {
            restrict: "M",
            link: function() {
                alert("See You Later!");
            }
        };
    })
    .directive("entering", function() {
        return function(scope, element, attrs) {
            element.bind("mouseenter", function() {
                element.addClass(attrs.entering);
                console.log("Mouse has entered the div");      
            });
        };
    })
    .directive("leaving", function(){
        return function(Scope, element, attrs) {
            element.bind("mouseleave", function() {
                element.removeClass(attrs.entering);
                console.log("Mouse has left the div");
            });
        };
    })
    .directive('clock', function() {
        return {
            restrict: 'E',
            scope: {
                timezone: '@'
            },
            template: "<div>12:00pm {{timezone}}</div>"
        };
    })
    .directive('panel', function() {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                title: '@'
            },
            template: "<div style='border: 3px solid #000000'>" +
                      "<div class='alert-box'>{{title}}</div>" + 
                      "<div ng-transclude></div></div>"
        };
    })
    .directive('country', function() {
        return {
            restrict: 'E',
            controller: function () {
                this.makeAnnouncement = function (message) {
                    console.log("Country says: " + message);
                };
            }
        };
    })
    .directive('state', function() {
        return {
            restrict: 'E',
            controller: function () {
                this.makeLaw = function (law) {
                    console.log("Law: " + law);
                };
            }
        };
    })
    .directive('city', function () {
        return {
            restrict: 'E',
            require: ['^country', '^state'],
            link: function (scope, element, attrs, Controllers) {
                Controllers[0].makeAnnouncement("This city rocks");
                Controllers[1].makeLaw("Jump Higher");
            }
        };
    })
    .controller('ChoreController', function($scope) {
        $scope.logChore = function(chore) {
            alert(chore + " is done!");
        };
    })
    .directive('kid', function() {
        return {
            restrict: 'E',
            scope: { 
                done: "&"
            },
            template: '<input type="text" ng-model="chore">' + 
                      '{{chore}}' +
                      '<div class="button" ng-click="done({chore: chore})">I\'m done</div>'
        };
    })
    .controller("AppController", function ($scope) {
        $scope.ctrlFlavor = "blackberry";
    $scope.callHome = function (message) {
        alert(message);
    };
    })
    .directive("drink", function () {
        return {
            scope: {
                flavor: "@"
            },
            template: '<div>{{flavor}}</div',
//            link: function(scope, element, attrs) {
//                scope.flavor = attrs.flavor;
//            }
        };
    })
    .directive("drunk", function () {
        return {
            scope: {
                flavor: "="
            },
            transclude: true,
            template: '<input type="text" ng-model="flavor"><ng-transclude></ng-transclude>',
        };
    })
    .directive('phone', function () {
        return {
            scope: {
                dial: "&"
            },
            template: '<input type="text" ng-model="value">' +
                      '<div class="button" ng-click="dial({message:value})">' +
                      'Call Home!</div>',
        }
    })
    .controller('PhoneController', function($scope) {
        $scope.leaveVoiceMail = function(number, message) {
            alert('Number: ' + number + ' said: '+ message );
        };
    })
    .directive('call', function() {
        return {
            restrict: 'E',
            scope: {
                number: '@',
                network: '=',
                makeCall: '&'
            },
            templateUrl: 'phone.html',
            link: function(scope) {
                scope.networks= ["Verizon", "AT&T", "Sprint"];
                scope.network = scope.networks[0];
            }
        };
    });