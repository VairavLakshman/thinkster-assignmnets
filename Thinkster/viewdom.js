var app = angular.module("app", []);
app.config(function($logProvider) {
    $logProvider.debugEnabled(false);
});
app.run(function($rootScope, $log) {
    $rootScope.$log = $log;
});
app.directive("dumbPassword", function () {
    var validElement = angular.element('<div>{{model.input}}</div>');
    var link = function(scope) {
                    scope.$watch("model.input", function (value) {
                        if(value === "password") {
                            validElement.toggleClass("alert-box alert");
                        }
                    });
                };
    return {
        restrict: "E",
        replace: true,
        templateUrl: "dumbpass.html",
        compile: function(tElem) {
            tElem.append(validElement);
            return link; 
        }
    };
});