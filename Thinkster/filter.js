function TestController() {
    var self = this;
    self.myString = "Hello World";
    self.people = [
        {
            name: "Eric Simons",
            born: "Chicago"
        },
        {
            name: "Albert Pai",
            born: "Taiwan"
        },
        {
            name: "Matthew Greenster",
            born: "Virginia"
        },
        {
            name: "Vairav Lakshmanan",
            born: "India"
        }
    ];
}
function CapitalizeFilter() {
    return function (text) {
        return text.toUpperCase();
    }
}

angular.module('app', [])
    .controller('TestController', TestController)
    .filter('capitalize', CapitalizeFilter);