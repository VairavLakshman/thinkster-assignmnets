(function () {
    function TestController($templateCache) {
        this.user = {name: 'Blake'};
        console.log($templateCache.get('test.html'));
    }
    angular.module('app', ['ngRoute'])
    .config(function($routeProvider) {
        $routeProvider.when('/', {
            controller: 'TestController as test',
//            template: 'Hello {{ test.user.name }}!'
            templateUrl: "test.html"
        })
        .otherwise('/');
    })
    .controller("TestController", TestController)
    .run(function ($templateCache) {
        $templateCache.put('test.html', 'Hello {{ test.user.name }}!');
    });
})()