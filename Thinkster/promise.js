function getData($timeout, $q) {
    return function() {
        return $q(function(resolve, reject) {
            $timeout(function() {
                resolve(Math.floor(Math.random() * 10));
//                if(Math.round(Math.random())) {
//                    resolve('Data Received!');
//                } else {
//                    reject("Oh no an error! try again");
//                }
            }, 2000);
        });
//        var defer = $q.defer();
//        $timeout(function() {
//            if(Math.round(Math.random())) {
//                defer.resolve('data received!');
//            } else {
//                defer.reject('Oh no an error! try again');
//            }
//        }, 2000);
//        return defer.promise;
    };
}
angular.module('app', [])
    .factory('getData', getData)
    .run(function(getData) {
        var promise = getData()
            .then(function(num) {
                console.log(num);
                return num * 2;
            })
            .then(function(num) {
                console.log(num);
            });
//            }, function(error) {
//                console.log(error);
//            })
//            .finally(function() {
//                console.log('Finished at:', new Date())
//            });
    });